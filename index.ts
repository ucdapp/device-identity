import { Uint64BE } from 'int64-buffer';

/**
 * Class used to seperate and compare data in a device identity, and to do so with less CPU power.
 * This class is currently for use with Alexa skills, but can be used for other purposes.
 * Currently this class is used for 2 situations in an Alexa skill:
 * 1) Get the address for a device out of its EndpointId, received from Alexa Skill Directive.
 * 2) Compare that address numerically to a received-from-device-cloud device address:
 *    convert received-from-device-cloud device address to a number and compare with number from Alexa Directive.
 *
 * An Identity has the following range:
 *                00 00 00 00 00 00 00 00 -
 *                00 1F FF FF FF FF FF FF
 *
 * Inside this range are different Identity components:
 * Index:         00 {1F} FF FF FF FF FF FF - Device Part Index - 1 Byte. Possible values: 00 - 1F
 * (NOT USED for Tundra API-based Alexa skills)
 *
 * MAC ADDRESS:   00 1F {FF FF FF FF FF FF} - 6 Bytes.  Possible values: 00 00 00 00 00 00 - FF FF FF FF FF FF
 *
 * MAC Address can be seperated into:
 * Organization:  00 1F {FF FF FF} FF FF FF - 3 bytes  - represents company or organization who issued MAC address
 * Code:          00 1F FF FF FF {FF FF FF} - 3 bytes  - represents unique device ID
 *
 * NOTE that for Tundra API-based Alexa skills, *no index* is to be found in this Identity: identity is managed outsideof this class.
 * Due to this, in the future, it might make more sense for Tundra Alexa skills to switch to another class for performing these comparisons.
 *
 */
export default class Identity {
  protected readonly mask = {
    block: 0x0000000000ffffff, // Used to get address out of a 12 byte identity
    index: 0x000000000000001f, // Used to make sure a 1 byte index only only contains values 00 - 1F
    multicast: 0x010000,
    locally: 0x020000
  };

  constructor(code: number, organization: number = 0, index: number = 0) {
    this.code = code;
    this.organization = organization;
    this.index = index;
  }

  protected _code: number = 0;

  get code() {
    return this._code;
  }

  /**
   * Save a 3-byte code value.
   */
  set code(value: number) {
    // noinspection NonShortCircuitBooleanExpressionJS
    this._code = value & this.mask.block; // filter out unneeded bytes to retrieve passed-in code
  }

  protected _index: number = 0;

  get index() {
    return this._index;
  }

  /**
   * Save a 1-byte index value.
   */
  set index(value: number) {
    // noinspection NonShortCircuitBooleanExpressionJS
    this._index = value & this.mask.index; // filter
  }

  protected _organization: number = 0;

  get organization() {
    return this._organization;
  }

  /**
   * Save a 3-byte index value.
   */
  set organization(value: number) {
    // noinspection NonShortCircuitBooleanExpressionJS
    this._organization = value & this.mask.block;
  }

  /**
   * Convert number or string to Identity class
   * @param item number or string to convert
   */
  static from(item: number | string) {
    const data = 'number' === typeof item ? item : parseInt(item, 16);
    const buffer = new Uint64BE(data).toBuffer();
    const code = buffer.readUIntBE(5, 3);
    const organization = buffer.readUIntBE(2, 3);
    const index = buffer.readUInt8(1);
    return new Identity(code, organization, index);
  }

  static validate(item: any) {
    if ('number' === typeof item) {
      return Number.isSafeInteger(item) && 0 <= item;
    }

    if (false === /^([0-9a-fA-F]+)$/.test(item)) {
      return false;
    }

    try {
      const data = parseInt(item, 16);
      return Number.isSafeInteger(data);
    } catch (error) {
      return false;
    }
  }

  formatCode() {
    return this.code.toString(16).toUpperCase();
  }

  formatIndex() {
    return this.hex(this.index, 2).toUpperCase();
  }

  isLocally() {
    // noinspection NonShortCircuitBooleanExpressionJS
    return Boolean(this.organization & this.mask.locally);
  }

  isMulticast() {
    // noinspection NonShortCircuitBooleanExpressionJS
    return Boolean(this.organization & this.mask.multicast);
  }

  toNumber() {
    return parseInt(this.toString(), 16);
  }

  /**
   * Convert numeric Identity components to a string
   */
  toString() {
    const organization = this.hex(this.organization, 6);
    const code = this.hex(this.code, 6);
    const index = this.hex(this.index, 4);
    return (index + organization + code).toUpperCase();
  }

  // noinspection JSMethodCanBeStatic
  protected hex(value: number, length: number) {
    const text = value.toString(16);
    const count = length > text.length ? length - text.length : 0;
    return ('0'.repeat(count) + text).substr(0, length);
  }
}
